Overview
========
This module provides an Input Filter with Karmacracy service integration to
short URLs with useful statistics of shared links and also funny gamification
badges and rewards.

INSTALLATION & ADMINISTRATION
============================
Enable Karmacracy Filter module on Administration > Modules. Then add the filter
to an existing text format or a new one on Administration > Configuration > Text
formats.

You must grant permission to the roles you want to let use Karmacracy Filter on
Administration > People > Permissions.

CONFIGURATION
=============
After enabling the module, new fields will be shown to users on user editing
form page to configure it. The credentias configuration is per user, so each
user needs a Karmacracy Username and App key to use it.

USAGE
=====
After enabling it on his or her user editing form page, the user only has to 
select a text format that has attached Karmacracy Filter.
